package de.iiliadis.todo_andro.main;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;
import de.iiliadis.todo_andro.Database.ConnClass;
import de.iiliadis.todo_andro.R;

public class Overlay extends Activity {

    //Define a value to hold the todoname
    private String todoName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.overlay);

        todoName = getIntent().getExtras().getString("todoName");

        //Set the todoname in the respective Textview
        TextView textView = findViewById(R.id.todoname);
        textView.setText(todoName);
    }

    public void deleteTodo(View v){

        //Access the database to delete the todoItem
        ConnClass connClass = ConnClass.getInstance();
        if(connClass.deleteTodo(todoName)){

            //If this returns true the operation was successful
            Toast.makeText(this, "Todo deleted", Toast.LENGTH_LONG).show();
        }else{

            //If the operation was not successful show a Toast
            Toast.makeText(this, "Something went wrong\nPlease try again", Toast.LENGTH_LONG).show();
        }

        //Close this dialoge
        finish();
    }
}
