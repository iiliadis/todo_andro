package de.iiliadis.todo_andro.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import de.iiliadis.todo_andro.Database.ConnClass;
import de.iiliadis.todo_andro.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set the View and show a little welcome note
        setContentView(R.layout.activity_main);
        Toast.makeText(this, "Welcome to the App :)", Toast.LENGTH_LONG).show();
    }

    public void createNewEntry(View v){

        //Get the Text from the TextView and check weather it is empty
        //An empty Textfield is not accepted
        EditText editText = findViewById(R.id.new_entry);
        String todoText = editText.getText().toString();

        if(!todoText.isEmpty()){

            //Connect to the database via the DatabaseClass and add the new Entry
            //If something went wrong the DatabaseClass will return false
            //In that case inform the user
            ConnClass connClass = ConnClass.getInstance();
            if(connClass.createNewTodo(todoText)){
                Toast.makeText(this, "New Todo added!", Toast.LENGTH_LONG).show();
                editText.setText("");
            }else{
                Toast.makeText(this, "UUps... something went wrong :(", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this, "Please name your new Todo", Toast.LENGTH_LONG).show();
        }
    }

    public void showTodos(View v){

        //start the respective Activity for the ListView of all todos
        startActivity( new Intent (this, List.class));
    }
}