package de.iiliadis.todo_andro.Model;

public class Todo {

    private final int id;
    private final String name;

    public Todo(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}