package de.iiliadis.todo_andro.Database;

import android.os.StrictMode;
import android.util.Log;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import de.iiliadis.todo_andro.Model.Todo;

public class ConnClass {

    //private attribute of this class for Singleton
    private static ConnClass connClass;

    //Define the private attributes for database connnection
    private Connection connection;
    private final String dbUrl = "jdbc:mysql://192.168.2.102/todo";
    private final String dbUser = "Jeder";
    private final String dbPassword = "";

    private ConnClass() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException classEx) {
            Log.v("Error", classEx.getMessage());
        }
    }

    public ArrayList<Todo> getAllTodos() {

        //predefine an Arraylist which holds the returned value
        ArrayList<Todo> ret = new ArrayList<>();

        try {
            //first open connection to database
            connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);

            //SELECT Query to get all todos of this user from database
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM todos");

            while (rs.next()) {

                //assign the respective values to variables and create new Objects to add to the Arraylist
                int id = rs.getInt(1);
                String name = rs.getString(2);

                Todo todo = new Todo(id, name);
                ret.add(todo);
            }

            //before return close database connection again
            st.close();
            rs.close();
            connection.close();

            //return the created Arraylist
            return ret;

        } catch (SQLException sqlEx) {
            Log.v("Error", sqlEx.getMessage());
            return null;
        }
    }

    public boolean createNewTodo(String todoName) {
        //helping variable to return the respective bool value
        boolean okay = false;

        try {
            //first open connection to database
            connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);

            //We made sure that all parameters are not empty in the calling class or method

            //Now make INSERT Query and see if an error is thrown
            PreparedStatement pSt = connection.prepareStatement("insert into todos(`name`) values(?)");
            pSt.setString(1, todoName);

            //execute the insert
            pSt.execute();

            //Now we check if the todoItem was inserted correctly by selecting it
            //Create a select statement where all the given parameters are queried
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM todos WHERE name='"+todoName+"'");

            while (rs.next()) {
                //double check if given values are okay and set the helping variable
                String name = rs.getString(2);

                okay = name.equals(todoName);
            }

            //before return close database connection again
            pSt.close();
            st.close();
            rs.close();
            connection.close();

            //This will tell if the user was inserted correctly
            return okay;

        } catch (SQLException sqlEx) {
            Log.v("Error", sqlEx.getMessage());
            return false;
        }
    }

    public boolean deleteTodo(String todoName) {
        //helping variable to return the respective bool value
        boolean okay;

        try {
            //first open connection to database
            connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);

            //Now make DELETE Query and see if an error is thrown
            //SELECT Query to get all todos of this user from database
            PreparedStatement pSt = connection.prepareStatement("DELETE FROM todos WHERE name=(?)");
            pSt.setString(1, todoName);

            //execute the delete
            pSt.execute();

            //Now we check if the todoItem was deleted correctly by selecting it
            //Create a select statement where all the given parameters are queried
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM todos WHERE name='"+todoName+"'");

            okay = rs.next();

            //before return close database connection again
            pSt.close();
            st.close();
            rs.close();
            connection.close();

            //This will tell if the user was deleted correctly
            //If rs.next() returns false, the operation was successful
            //So return true, if rs.next() returns false and
            //return false, if rs.next() returns true
            return !okay;

        } catch (SQLException sqlEx) {
            Log.v("Error", sqlEx.getMessage());
            return false;
        }
    }

    public static ConnClass getInstance() {
        if (connClass == null) {
            connClass = new ConnClass();
        }
        return connClass;
    }
}