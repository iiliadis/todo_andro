package de.iiliadis.todo_andro.main;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import de.iiliadis.todo_andro.Database.ConnClass;
import de.iiliadis.todo_andro.Model.Todo;
import de.iiliadis.todo_andro.R;

public class List extends ListActivity {

    //Define a variable for the database connection Class
    private ConnClass connClass;

    //Define the ListAdapter here
    private ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.my_todos); //Does not work...WHY?

        //Define a listItems Arraylist
        ArrayList<String> listItems = new ArrayList<>();

        //Get all the todos from the database
        connClass = ConnClass.getInstance();
        ArrayList<Todo> todos = connClass.getAllTodos();

        for (Todo todo : todos) {
            listItems.add(todo.getName());
        }

        //Set the Listview adapter with the created and filled Arraylist
        String[] items = new String[listItems.size()];
        listItems.toArray(items);

        adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,items);
        setListAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Define a listItems Arraylist
        ArrayList<String> listItems = new ArrayList<>();

        //Get all the todos from the database
        connClass = ConnClass.getInstance();
        ArrayList<Todo> todos = connClass.getAllTodos();

        for (Todo todo : todos) {
            listItems.add(todo.getName());
        }

        //Set the Listview adapter with the created and filled Arraylist
        String[] items = new String[listItems.size()];
        listItems.toArray(items);

        adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,items);
        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        //Fetch the todoname of the clicked todoitem
        String todoName = ((TextView)v).getText().toString();
        Intent intent = new Intent(this, Overlay.class);
        intent.putExtra("todoName", todoName);
        startActivity(intent);
    }
}